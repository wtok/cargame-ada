separate(Cargame.Obj_Parser)
procedure Get_Face(Split_Line                      : in     UB_String_Vector;
                   Output_VI, Output_NI, Output_TI : in out Vector_Of_UInt)
is
    function Extract_UInts(Str : Unbounded_String) return Vector_Of_UInt
        with Pre => (Length(Str) /= 0)
    is
        Modular_CSet : constant Character_Set := To_Set
            (Character_Range'(Low => '0', High => '9'));

        Ret     : Vector_Of_UInt;
        Split   : UB_String_Vector;
        Got_Idx : UInt;
        Unused  : Positive;
    begin
        Split := Split_Into_Tokens(Str, Modular_CSet);
        for S of Split loop
            UIO.Get(To_String(S), Got_Idx, Last => Unused);
            Ret.Append(Got_Idx);
        end loop;
        return Ret;
    end Extract_UInts;

    Indices_On_Line : Vector_Of_UInt;
    VI, TI, NI      : Vector_Of_UInt;
    Values_Per_Face : constant Positive
        := Positive(Extract_UInts(Split_Line(2)).Length);

begin

    for I in 2 .. Positive(Split_Line.Length) loop
        declare
            Indices : constant Vector_Of_UInt
                := Extract_UInts(Split_Line.Element(I));
        begin
            for Idx of Indices loop
                Indices_On_Line.Append(Idx);
            end loop;
        end;
    end loop;

    pragma Assert(Positive(Indices_On_Line.Length) mod Values_Per_Face = 0,
                  "Total indices on each face line must"
                      & " be a multiple of indices per face.");

    case Values_Per_Face is
        when 1 => -- v format. Only vertices specified.
            for I in Indices_On_Line.Iterate loop
                VI.Append(Indices_On_Line(I));
            end loop;
        when 2 => -- v//n format. Vertices and normals.
            for I in Indices_On_Line.Iterate loop
                case (UInt_Vectors.To_Index(I) mod 2) is
                    when 1 =>
                        VI.Append(Indices_On_Line(I));
                    when 0 =>
                        NI.Append(Indices_On_Line(I));
                    when others =>
                        raise Invalid_Obj_File
                            with "Weird number of indices in face line.";
                end case;
            end loop;
        when 3 => -- v/t/n format. Vertices, texcoords, normals.
            for I in Indices_On_Line.Iterate loop
                case (UInt_Vectors.To_Index(I) mod 3) is
                    when 1 =>
                        VI.Append(Indices_On_Line(I));
                    when 2 =>
                        TI.Append(Indices_On_Line(I));
                    when 0 =>
                        NI.Append(Indices_On_Line(I));
                    when others =>
                        raise Invalid_Obj_File
                            with "Weird number of indices in face line.";
                end case;
            end loop;
        when others =>
            raise Invalid_Obj_File
                with "Face component wasn't v, v//n, or v/t/n.";
    end case;

    if VI.Length > 3 then
        -- TODO: Combine vertices into a number of triangles. Obj format
        --  specifies any arbitrary amount of face components can be specified,
        --  so it doesn't have to be triangles/quads. It could be an octagon or
        --  whatever.
        --
        -- NOTE(wtok, 2018-03-29): It seems that in practice, faces are usually
        --  either triangles or squares. So just implementing quads -> triangles
        --  would get us quite far.
        raise Invalid_Obj_File
            with "We can't do anything more than triangles right now.";
    end if;

    Output_VI.Append(VI);
    Output_TI.Append(TI);
    Output_NI.Append(NI);

end Get_Face;
