with System;
with Ada.Text_IO; use Ada.Text_IO;
with GL.Pixels;
with GL.Objects.Textures.Targets;

with Ada.Unchecked_Conversion;

-- NOTE(wtok, 2018-04-18): This relies on stb_image.h, which happens to be included with SOIL, which
--  happens to be included in OpenGLAda. I couldn't get SOIL to work, but stb_image is ridiculously
--  simple to use, so here it is.

package body Cargame.Texture_Loader is
        use GL.Objects.Textures;
        use GL.Objects.Textures.Targets;
        use GL.Objects.Textures.Targets.Texture_2D_Target;

        procedure Load_Texture(Filename : in String;
                               Tex      : in out GL.Objects.Textures.Texture)
        is
                Image_Data : Chars_Ptr;
                Width, Height, Channels_In_File : Int := -1;
                Desired_Channels : constant Int := 3;

                Filename_C : Chars_Ptr := New_String(Filename);

                function To_Image_Source is
                        new Ada.Unchecked_Conversion(Chars_Ptr, Image_Source);

        begin
                Texture_2D.Bind(Tex);

                Image_Data := STBI_Load(Filename         => Filename_C,
                                        X                => Width,
                                        Y                => Height,
                                        Channels_In_File => Channels_In_File,
                                        Desired_Channels => Desired_Channels);

                Free(Filename_C);

                --  Put_Line("Loaded image with (Width => " & Int'Image(Width)
                --               & ", Height => " & Int'Image(Height)
                --               & ", Channels => " & Int'Image(Channels_In_File)
                --               & ")");

                pragma Assert(Width > 0 and Height > 0 and Channels_In_File > 0,
                              "STBI_Load error: " & (if STBI_Failure_Reason /= Null_Ptr
                                                         then Value(STBI_Failure_Reason)
                                                         else "UNKNOWN"));

                Texture_2D.Load_From_Data(Level           => Mipmap_Level(0),
                                          Internal_Format => GL.Pixels.RGB,
                                          Width           => Width,
                                          Height          => Height,
                                          Source_Format   => GL.Pixels.RGB,
                                          Source_Type     => GL.Pixels.Unsigned_Byte,
                                          Source          => To_Image_Source(Image_Data));

                Generate_Mipmap(Texture_2D);

                STBI_Image_Free(Image_Data);

                --  Texture_2D.Bind(0);

        end Load_Texture;

end Cargame.Texture_Loader;
