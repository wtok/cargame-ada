with GL.Types;

package body Cargame.Physics is

    function Reduplicate(Vec : Vector3_Vector; Indices : UInt_Vector) return Vector3_Vector is
        use GL.Types;
    begin
        return Ret : Vector3_Vector do
            for Idx of Indices loop
                Ret.Append(Vec.Element(Int(Idx)));
            end loop;
        end return;
    end Reduplicate;

    function Create_Collision_Mesh(Model : in Obj_Data) return Collision_Mesh is
        Min : Vector3 := (Model.Unique_Vertices.Element(1));
        Max : Vector3 := (Model.Unique_Vertices.Element(1));
    begin
        for Vtx of Model.Unique_Vertices loop
            for I in Vtx'Range loop
                if Vtx(I) < Min(I) then Min(I) := Vtx(I); end if;
                if Vtx(I) > Max(I) then Max(I) := Vtx(I); end if;
            end loop;
        end loop;

        return AABB'(Position => <>,
                     Size     => (X => (Max(X) - Min(X)),
                                  Y => (Max(Y) - Min(Y)),
                                  Z => (Max(Z) - Min(Z))));
    end Create_Collision_Mesh;

    function Meshes_Collide(A, B : Collision_Mesh) return Boolean is
    begin
        return not  (A.Position(X) >= B.Extent(X) or else A.Extent(X) <= B.Position(X))
            or else (A.Position(Y) >= B.Extent(Y) or else A.Extent(Y) <= B.Position(Y))
            or else (A.Position(Z) >= B.Extent(Z) or else A.Extent(Z) <= B.Position(Z));
    end Meshes_Collide;

end Cargame.Physics;
