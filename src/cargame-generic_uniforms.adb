with GL.Uniforms;
with GL.Types;
with Ada.Text_IO;

package body Cargame.Generic_Uniforms is

   package body Generic_Uniform is
      Uniform_Index : Uniform := -1;
      Current_Value : Uniform_Type;

      use GL.Uniforms;
      use GL.Types;
      use Ada.Text_IO;

      function Initialised return Boolean is (Uniform_Index /= -1);

      procedure Initialise(GL_Program : in Program) is
      begin
	 if not Initialised then
	    pragma Assert(GL_Program.Initialized);
	    Uniform_Index := GL_Program.Uniform_Location(Name);
	 end if;
	 -- Re-initialisation is a no-op.
      end Initialise;

      function Image return String is (Name);

      function Get return Uniform_Type is (Current_Value);

      procedure Set_Without_Sending(Val : in Uniform_Type) is
      begin
	 Current_Value := Val;
      end Set_Without_Sending;

      procedure Send_To_GPU is
      begin
	 Set_Procedure(Uniform_Index, Current_Value);
      end Send_To_GPU;

      procedure Set(Val : in Uniform_Type) is
      begin
	 Set_Without_Sending(Val);
	 Send_To_GPU;
      end Set;

      procedure Initialise_With_Value(GL_Program : in Program;
				      Val        : in Uniform_Type) is
      begin
	 Initialise(GL_Program);
	 Set(Val);
      end Initialise_With_Value;

   end Generic_Uniform;

end Cargame.Generic_Uniforms;
