with Ada.Directories;
with Ada.Containers;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with GL.Types;
with GL.Objects.Textures; use GL.Objects.Textures;

with Cargame.Globals;
with Cargame.Types;
with Cargame.Util;    use Cargame.Util;
with Cargame.Vectors; use Cargame.Vectors;

package Cargame.Obj_Parser is

   use GL.Types;
   use GL.Types.Singles;
   use Cargame.Types;
   use Ada.Containers;

   Invalid_Obj_File : exception;
   Invalid_Mtl_File : exception;

   -- Parsing helper functions.
   --
   -- NOTE(wtok, 2018-04-01): These functions are really intended for
   --  use in Obj/Mtl files, where there's some token as the first
   --  thing on a line. e.g. "v 0.0 1.0 2.0" has 4 elements, so
   --  Get_Vector3 expects 4 elements and ignores the first one.

   function Get_Vector3(Split_Line : in UB_String_Vector) return Vector3
       with Pre  => (Split_Line.Length = 4
                       and then To_String(Split_Line(1)) in "v" | "vn" | "Ks" | "Kd" | "Ka"),
            Post => (Is_Valid(Get_Vector3'Result));

   function Get_Vector2(Split_Line : in UB_String_Vector) return Vector2
       -- NOTE(wtok,2018-04-06): Sometimes texcoords are specified as
       --  cubemaps, and it may make sense to only care about the x,y
       --  components of a texcoord. So the length could be greater
       --  than or equal to 3.
       with Pre  => (Split_Line.Length >= 3
                       and then To_String(Split_Line(1)) in "vt"),
            Post => (Is_Valid(Get_Vector2'Result));

   --------------
   -- Obj_Data --
   --------------

   type Obj_Data is record
       Unique_Vertices : Vector_Of_Vector3;
       Unique_Normals  : Vector_Of_Vector3;
       Unique_TexCrds  : Vector_Of_Vector2;
       Vertex_Indices  : Vector_Of_UInt;
       Normal_Indices  : Vector_Of_UInt;
       TexCrd_Indices  : Vector_Of_UInt;
       Materials       : Vector_Of_Material;
   end record
       -- There really are a lot of ways the data could be messed up. It would
       -- be very sensible to make sure we have one condition per line here,
       -- since failed assertions only give a line number as a hint.
       with Dynamic_Predicate =>
                    (Obj_Data.Normal_Indices.Length      = Obj_Data.Vertex_Indices.Length)
           and then (Obj_Data.TexCrd_Indices.Length      = Obj_Data.Vertex_Indices.Length)
           and then (Obj_Data.Normal_Indices.First_Index = Obj_Data.Vertex_Indices.First_Index)
           and then (Obj_Data.TexCrd_Indices.First_Index = Obj_Data.Vertex_Indices.First_Index)
           and then (for all V of Obj_Data.Unique_Vertices => Is_Valid(V))
           and then (for all V of Obj_Data.Unique_Normals  => Is_Valid(V))
           and then (for all V of Obj_Data.Unique_TexCrds  => Is_Valid(V))
           and then (for all I of Obj_Data.Vertex_Indices
                         => (I in   UInt(Obj_Data.Unique_Vertices.First_Index)
                                 .. UInt(Obj_Data.Unique_Vertices.Last_Index)))
           and then (for all I of Obj_Data.Normal_Indices
                         => (I in   UInt(Obj_Data.Unique_Normals.First_Index)
                                 .. UInt(Obj_Data.Unique_Normals.Last_Index)))
           and then (for all I of Obj_Data.TexCrd_Indices
                         => (I in   UInt(Obj_Data.Unique_TexCrds.First_Index)
                                 .. UInt(Obj_Data.Unique_TexCrds.Last_Index)))
           and then (for all M of Obj_Data.Materials => (M.First_Index /= -1))
           and then (for all M of Obj_Data.Materials => (M.Final_Index /= -1));

   function Parse(File_Path : in String) return Obj_Data
       with Pre => ((Ada.Directories.Exists(File_Path))
                    and then (Globals.GL_Program.Initialized));
end Cargame.Obj_Parser;
