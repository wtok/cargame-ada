-- C++ style vectors, not math vectors.

with Ada.Containers.Vectors;

with GL.Types;
with GL.Objects.Textures;
with Cargame.Types;

package Cargame.Vectors is

    use GL.Types;
    use GL.Types.Singles;
    use Cargame.Types;

    ------------------
    -- Vector types --
    ------------------

    package Single_Vectors         is new Ada.Containers.Vectors(Positive, Single);
    package Vector2_Vectors        is new Ada.Containers.Vectors(Positive, Vector2);
    package Vector3_Vectors        is new Ada.Containers.Vectors(Positive, Vector3);
    package UInt_Vectors           is new Ada.Containers.Vectors(Positive, UInt);
    package Texture_Vectors        is new Ada.Containers.Vectors(Positive, GL.Objects.Textures.Texture);
    package Radians_Vectors        is new Ada.Containers.Vectors(Positive, Cargame.Types.Radians);
    package Distance_Type_Vectors  is new Ada.Containers.Vectors(Positive, Cargame.Types.Distance_Type);
    package Position_Type_Vectors  is new Ada.Containers.Vectors(Positive, Cargame.Types.Position_Type);
    package Velocity_Type_Vectors  is new Ada.Containers.Vectors(Positive, Cargame.Types.Velocity_Type);
    package Material_Vectors       is new Ada.Containers.Vectors(Positive, Cargame.Types.Material);
    package Matrix3_Vectors        is new Ada.Containers.Vectors(Positive, Matrix3);
    package Matrix4_Vectors        is new Ada.Containers.Vectors(Positive, Matrix4);

    subtype Vector_Of_Single         is Single_Vectors.Vector;
    subtype Vector_Of_Vector2        is Vector2_Vectors.Vector;
    subtype Vector_Of_Vector3        is Vector3_Vectors.Vector;
    subtype Vector_Of_UInt           is UInt_Vectors.Vector;
    subtype Vector_Of_Texture        is Texture_Vectors.Vector;
    subtype Vector_Of_Radians        is Radians_Vectors.Vector;
    subtype Vector_Of_Distance_Type  is Distance_Type_Vectors.Vector;
    subtype Vector_Of_Position_Type  is Position_Type_Vectors.Vector;
    subtype Vector_Of_Velocity_Type  is Velocity_Type_Vectors.Vector;
    subtype Vector_Of_Material       is Material_Vectors.Vector;
    subtype Vector_Of_Matrix3        is Matrix3_Vectors.Vector;
    subtype Vector_Of_Matrix4        is Matrix4_Vectors.Vector;

    -----------------
    -- Array types --
    -----------------

    type Array_Of_Single         is array (Positive range <>) of aliased Single;
    type Array_Of_Vector2        is array (Positive range <>) of aliased Vector2;
    type Array_Of_Vector3        is array (Positive range <>) of aliased Vector3;
    type Array_Of_UInt           is array (Positive range <>) of aliased UInt;
    type Array_Of_Radians        is array (Positive range <>) of aliased Radians;
    type Array_Of_Distance_Type  is array (Positive range <>) of aliased Distance_Type;
    type Array_Of_Position_Type  is array (Positive range <>) of aliased Position_Type;
    type Array_Of_Velocity_Type  is array (Positive range <>) of aliased Velocity_Type;
    type Array_Of_Material       is array (Positive range <>) of aliased Material;
    type Array_Of_Matrix3        is array (Positive range <>) of aliased Matrix3;
    type Array_Of_Matrix4        is array (Positive range <>) of aliased Matrix4;

end Cargame.Vectors;
