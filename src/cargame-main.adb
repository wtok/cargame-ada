with Ada.Command_Line;
with Ada.Directories;
with Ada.Real_Time;            use Ada.Real_Time;
with Ada.Text_IO;              use Ada.Text_IO;

with GL;                       use GL;
with GL.Buffers;               use GL.Buffers;
with GL.Files;
with GL.Objects.Programs;      use GL.Objects.Programs;
with GL.Objects.Shaders;       use GL.Objects.Shaders;
with GL.Toggles;
with GL.Types;                 use GL.Types; use GL.Types.Singles;
with GL.Uniforms;              use GL.Uniforms;

with Glfw;
with Glfw.Input;               use Glfw.Input;
with Glfw.Windows;             use Glfw.Windows;
with Glfw.Windows.Context;     use Glfw.Windows.Context;
with Glfw.Windows.Hints;       use Glfw.Windows.Hints;

with Cargame.Globals;          use Cargame.Globals;
with Cargame.Renderables;      use Cargame.Renderables;
with Cargame.Types;            use Cargame.Types;
with Cargame.Uniforms;
with Cargame.Entity;           use Cargame.Entity;
with Cargame.Util;
with Cargame.Gameplay;

procedure Cargame.Main is

   A_Light  : constant Light := Light'(Position => (0.0, 0.0, 0.0),
                                       Velocity => (others => 0.0),
                                       Rotation => Radians(0.0),
                                       Ambient  => (1.0, 1.0, 1.0, 1.0),
                                       Diffuse  => (1.0, 1.0, 1.0, 1.0),
                                       Specular => (1.0, 1.0, 1.0, 1.0));

   Frame_T0 : Time;
   Frame_T1 : Time;

   package RVAOs is
      Car    : Renderable_VAO;
      Barrel : Renderable_VAO;
   end RVAOs;

begin

   Ada.Directories.Set_Directory
       (Ada.Directories.Containing_Directory(Ada.Command_Line.Command_Name));

   ----------------------------
   -- Set up GLFW and OpenGL --
   ----------------------------

   Util.Got_Here;

   Glfw.Init; -- Initialises both GLFW and OpenGL.

   Set_Minimum_OpenGL_Version(Major => 3, Minor => 3);
   Set_Forward_Compat(True);
   Set_Profile(Core_Profile);

   Init(Object => Globals.Main_Window,
        Title  => "Skeltal Racing",
        Width  => Globals.Main_Window_Width,
        Height => Globals.Main_Window_Height);

   pragma Assert(Main_Window.Initialized,
                 "Failed to initialise main window.");

   Make_Current(Main_Window);
   Set_Swap_Interval(0);

   Set_Color_Clear_Value(Globals.Background_Colour);

   -- Enable callbacks
   Main_Window.Enable_Callback(Glfw.Windows.Callbacks.Key);
   Main_Window.Enable_Callback(Glfw.Windows.Callbacks.Size);
   Main_Window.Enable_Callback(Glfw.Windows.Callbacks.Mouse_Position);
   Main_Window.Enable_Callback(Glfw.Windows.Callbacks.Mouse_Button);

   Util.Got_Here("Setting Up Shaders.");

   ---------------------------------
   -- Set up GL_Program (shaders) --
   ---------------------------------

   declare
      Vertex_Shader   : Shader(Kind => GL.Objects.Shaders.Vertex_Shader);
      Fragment_Shader : Shader(Kind => GL.Objects.Shaders.Fragment_Shader);

      Vertex_Path   : constant String := "../src/shaders/vert.glsl";
      Fragment_Path : constant String := "../src/shaders/frag.glsl";
      use Ada.Directories;
   begin
      pragma Assert(Exists(Vertex_Path),   "Couldn't find vertex shader.");
      pragma Assert(Exists(Fragment_Path), "Couldn't find fragment shader.");

      Initialize_ID(GL_Program);
      Initialize_ID(Vertex_Shader);
      Initialize_ID(Fragment_Shader);

      GL.Files.Load_Shader_Source_From_File(Vertex_Shader, Vertex_Path);
      GL.Files.Load_Shader_Source_From_File(Fragment_Shader, Fragment_Path);

      Compile(Vertex_Shader);
      Compile(Fragment_Shader);

      if not Vertex_Shader.Compile_Status then
         Put_Line("Log: " & Vertex_Shader.Info_Log);
         pragma Assert(False, "Failed to compile Vertex shader.");
      end if;

      if not Fragment_Shader.Compile_Status then
         Put_Line("Log: " & Fragment_Shader.Info_Log);
         pragma Assert(False, "Failed to compile Fragment shader.");
      end if;

      GL_Program.Attach(Vertex_Shader);
      GL_Program.Attach(Fragment_Shader);
      GL_Program.Link;

      if not GL_Program.Link_Status then
         Put_Line("Log: " & GL_Program.Info_Log);
         pragma Assert(False, "Failed to link shaders.");
      end if;
   end; -- shader setup

   Util.Got_Here("Shaders done.");

   GL_Program.Use_Program;
   GL.Toggles.Enable(GL.Toggles.Depth_Test);

   Util.Got_Here;

   -------------------------
   -- Initialise Uniforms --
   -------------------------

   Uniforms.Projection.Initialise_With_Value
     (GL_Program, Val => Perspective_Matrix(View_Angle   => Degrees(90.0),
                                            Aspect_Ratio => Globals.Aspect_Ratio,
                                            Near         => Globals.Near_Plane,
                                            Far          => Globals.Far_Plane));
   Uniforms.Camera_Transform.Initialise_With_Value
     (GL_Program, Val => Look_At(Camera_Position => (0.0, 2.0, -2.0),
                                 Target_Position => (others => 0.0),
                                 Up              => (Y => 1.0, others => 0.0)));
   Uniforms.Object_Transform.Initialise_With_Value(GL_Program, Identity4);
   Uniforms.CamObj_Transform.Initialise_With_Value(GL_Program, Identity4);
   Uniforms.Normal_Transform.Initialise(GL_Program);
   Uniforms.Diffuse_Map.Initialise_With_Value(GL_Program, Globals.Diffuse_Map_ID);
   Uniforms.Specular_Map.Initialise_With_Value(GL_Program, Globals.Specular_Map_ID);
   Uniforms.Material_Ambient.Initialise(GL_Program);
   Uniforms.Material_Shininess.Initialise(GL_Program);
   Uniforms.Light_Position.Initialise(GL_Program);
   Uniforms.Light_Ambient.Initialise(GL_Program);
   Uniforms.Light_Diffuse.Initialise(GL_Program);
   Uniforms.Light_Specular.Initialise(GL_Program);

   -- Send Light data
   A_Light.Send_Render_Data;

   -------------------------
   -- Initialise entities --
   -------------------------

   declare
      Car    : constant Renderable_VAO := Create_Renderable_VAO("../src/models/car-n.obj");
      Barrel : constant Renderable_VAO := Create_Renderable_VAO("../src/models/Barrel02.obj");
   begin
      -- Used in future
      RVAOs.Car    := Car;
      RVAOs.Barrel := Barrel;

      Gameplay.Entities := Entity_Vector_With
        ((
          (RVAO     => Car,
           Position => (0.0, 0.0, 0.0),
           Velocity => (others => 0.0),
           Scale    => 0.2,
           Rotation => 0.0,
           Is_Live  => True),
          (RVAO     => Barrel,
           Position => (0.0, 0.0, 10.0),
           Velocity => (others => 0.0),
           Scale    => 10.0,
           Rotation => 0.0,
           Is_Live  => True)
         ));
   end;

   Gameplay.Entities(2).Accelerate_Towards(Target         => (0.0, 0.0, 0.0),
                                           Time_To_Arrive => Seconds(5));

   Put_Line("Entities loaded and initialised.");

   --------------------
   -- Main game loop --
   --------------------

   Util.Got_Here("Before main loop.");

   loop

      -------------------
      -- Set deadlines --
      -------------------

      -- Evaluate Clock as the first thing in the frame.
      Frame_T0 := Clock;

      Next_Frame_Time := Frame_T0 + Frame_Interval;

      if Frame_T0 > Globals.Next_Input_Poll_Time then
         Glfw.Input.Poll_Events;
         Globals.Next_Input_Poll_Time
           := Globals.Next_Input_Poll_Time + Globals.Input_Poll_Interval;
      end if;

      -----------
      -- Frame --
      -----------

      exit when Main_Window.Should_Close;

      Clear(Buffer_Bits'(Depth => True, Color => True, others => <>));

      -- TODO:
      -- Clean_Up_Invisible_Obstacles; -- Unimplemented
      -- Increase_Player_Speed;        -- Unimplemented
      -- Maybe_Update_Top_Score;       -- Unimplemented

      -- Place procedural terrain

      -- TODO.
      Uniforms.Procedural_Terrain.Set(1); -- True.

      -- Terrains : Array_Of_Entity := Generate_Terrain_Segments(3);
      for T of Terrains loop
         if T.Is_Off_Screen then
            T.Move_To_Back_Of_Terrain_Queue;
         else
            -- TODO.
         end if;
      end loop;

      -- Update entities

      for E of Gameplay.Entities loop
         if E.Is_Live then
            E.Update;
            E.Render;
         end if;
      end loop;

      GL.Flush;
      Swap_Buffers(Main_Window);

      --------------------------
      -- Post-frame analytics --
      --------------------------

      Frame_T1 := Clock;

      if Frame_T1 > Next_Frame_Time then
         Put_Line("Missed frame duration target by "
                    & Util.Time_Span_Image(Frame_T1 - Next_Frame_Time));
      end if;

      -- Wait until frame deadline, if necessary.
      delay until Next_Frame_Time;

   end loop; -- Main loop

   Destroy(Main_Window);
   Put_Line("Thanks for playing!");

   Glfw.Shutdown;
end Cargame.Main;
