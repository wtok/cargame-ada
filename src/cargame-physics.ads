with GL;
with GL.Types;
with Cargame.Types;
with Cargame.Obj_Parser;

package Cargame.Physics is

    use GL;
    use GL.Types;
    use GL.Types.Singles;
    use Cargame.Types;
    use Cargame.Obj_Parser;

    type AABB is tagged record
        Position, Size : Vector3;
    end record;

    function Extent(Box : in AABB; Axis : in GL.Index_3D) return Single
        is (Box.Position(Axis) + Box.Size(Axis));

    -- Abstraction. For now it's just a bounding box. In future maybe it'll be more complicated, if
    --  it needs to be.
    subtype Collision_Mesh is AABB;

    function Create_Collision_Mesh(Model : Obj_Data) return Collision_Mesh;
    function Meshes_Collide(A, B : Collision_Mesh) return Boolean with Inline;
    function Collides_With(A, B : Collision_Mesh) return Boolean
        -- So we can write "if A.Collides_With(B)" and such.
        renames Meshes_Collide;

end Cargame.Physics;
