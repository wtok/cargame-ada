with Ada.Directories;
with Interfaces.C;
with Interfaces.C.Strings;
with GL.Types;
with GL.Objects.Textures;

package Cargame.Texture_Loader is
        use Interfaces.C.Strings;
        use GL.Types;

        -- stbi_uc *stbi_load (char const *filename,
        --                     int *x, int *y, int *channels_in_file,
        --                     int desired_channels);
        function STBI_Load(Filename               : in     Chars_Ptr;
                           X, Y, Channels_In_File :    out GL.Types.Int;
                           Desired_Channels       : in     GL.Types.Int)
                return Chars_Ptr
            with Import, Convention => C, External_Name => "stbi_load",
                 Post => Interfaces.C.Is_Nul_Terminated(Value(STBI_Load'Result));

        procedure STBI_Image_Free(Data : in Chars_Ptr)
            with Import, Convention => C, External_Name => "stbi_image_free";

        function STBI_Failure_Reason return Interfaces.C.Strings.Chars_Ptr
            with Import, Convention => C, External_Name => "stbi_failure_reason";

        procedure Load_Texture(Filename : in String;
                               Tex      : in out GL.Objects.Textures.Texture)
            with Pre => Ada.Directories.Exists(Filename);

end Cargame.Texture_Loader;
