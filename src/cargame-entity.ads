with Ada.Containers.Vectors;
with Ada.Real_Time;

with GL.Types;
with GL.Types.Colors;

with Cargame.Types;
with Cargame.Obj_Parser;
with Cargame.Renderables;
with Cargame.Vectors;

--
-- Cargame.Entity
--
-- The API here is intended to be really simple until it needs to be
--  more complicated. I've defined an Entity and Entity_Vector. When
--  you want to allocate a number of Entities, declare an
--  Entity_Vector and append to your heart's content.
--
-- The vector "Entities" is declared in here. For the moment that'll contain 
--  every entity in the game.
--  

package Cargame.Entity is

   use Cargame.Renderables;
   use Cargame.Types;
   use Cargame.Obj_Parser;
   use GL.Types;
   use GL.Types.Singles;
   use GL.Types.Colors;

   type Entity is tagged record
      RVAO      : Renderable_VAO := Default_RVAO;
      Position  : Position_Type  := (0.0, 0.0, 0.0);
      Velocity  : Velocity_Type  := (0.0, 0.0, 0.0);
      Rotation  : Radians        := 0.0;
      Scale     : Single         := 1.0;
      Is_Live   : Boolean        := True;
   end record;

   procedure Update  (E : in out Entity) with Inline;
   procedure Render  (E : in     Entity)
      with Pre => Is_Actually_Renderable(E.RVAO);
   procedure Move    (E        : in out Entity;
                      Distance : in     Distance_Type) with Inline;
   procedure Place_At(E        : in out Entity;
                      Position : in     Position_Type) with Inline;
   procedure Rotate  (E        : in out Entity;
                      Angle    : in     Radians) with Inline;
   procedure Rotate  (E        : in out Entity;
                      Angle    : in     Degrees) with Inline;

   use Ada.Real_Time;

   procedure Accelerate_Towards(E              : in out Entity;
                                Target         : in     Position_Type;
                                Time_To_Arrive : in     Time_Span) with Inline;

   procedure Advance_Obstacle(E : in out Entity);
   function Entities_Collide(A, B : in Entity) return Boolean;
   function Entity_Is_Off_Screen(E : in Entity) return Boolean;

   type Change_Direction is (Left, Right);
   procedure Change_Lanes(E : in out Entity; Direction : Change_Direction);

   package Entity_Vectors is
      new Ada.Containers.Vectors(Positive, Entity);
   subtype Vector_Of_Entity is Entity_Vectors.Vector;

   -- All entities in the game, I guess.
   Entities : Vector_Of_Entity;

   type Array_Of_Entity is
     array (Positive range <>) of aliased Entity;

   function Entity_Vector_With(Initial_Elements : in Array_Of_Entity)
                              return Vector_Of_Entity;

   function Append_Default_Entity(V : in out Vector_Of_Entity)
                                 return Natural;

   type Light is tagged record
      Position  : Position_Type  := (0.0, 0.0, 0.0);
      Velocity  : Velocity_Type  := (0.0, 0.0, 0.0);
      Rotation  : Radians        := 0.0;
      Ambient, Diffuse, Specular : Color;
   end record;

   procedure Send_Render_Data(L : in Light);

end Cargame.Entity;
