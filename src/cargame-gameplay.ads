with Cargame.Entity;

package Cargame.Gameplay is
   Num_Lanes : constant Integer := 3;
   type Lane is new Integer range 1 .. Num_Lanes;

   procedure   Spawn_Obstacle(L : in Lane);

   procedure Clean_Up_Invisible_Entities;

end Cargame.Gameplay;
