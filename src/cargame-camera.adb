with Cargame.Types; use Cargame.Types;

package body Cargame.Camera is

        Previous_Known_Mouse_Position : Vector2 := (0, 0);

        procedure Update_From_Mouse_Position is
                use Cargame.Types.Single_Elementary_Functions;

                Eye_X : Vector3 := (View_Mtx(X)(X), View_Mtx(Y)(X), View_Mtx(Z)(X));
                Eye_Y : Vector3 := (View_Mtx(X)(Y), View_Mtx(Y)(Y), View_Mtx(Z)(Y));
                Eye_Z : Vector3 := (View_Mtx(X)(Z), View_Mtx(Y)(Z), View_Mtx(Z)(Z));
        begin
        end Update;

end Cargame.Camera;
